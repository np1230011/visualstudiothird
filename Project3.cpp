﻿
#include <iostream>

void FindOddNumbers (bool isOdd, int Limit)
{
    //Меняем bool на целочисленный тип
    int result = (isOdd) ? 1 : 0;
    //Если нужны нечетные, то начинаем выводить с единицы с шагом 2
    //Если нужны нечетны, начинаем выводить с 0 с шагом два
    for (int i = result; i < (Limit + 1); i+=2)
    {
        std::cout << i << "\n";
    }

 /*   if (isOdd)
    {
        for (int i = 0; i < (Limit + 1); i++)
        {
            if (i % 2 == 1)
            {
                std::cout << i << "\n";
            }
        }
    }
    else
    {
        for (int i = 0; i < (Limit + 1); i++)
        {
            if (i % 2 == 0)
            {
                std::cout << i << "\n";
            }
        }
    }*/
}

int main()
{
    int N = 10;
    FindOddNumbers(false, N);
}

